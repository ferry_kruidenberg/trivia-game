import VueRouter from "vue-router";
import Vue from "vue";
import StartScreen from "@/components/StartScreen";
import QuestionScreen from "@/components/QuestionScreen";
import ResultScreen from "@/components/ResultScreen";


Vue.use(VueRouter)

const routes = [
    {
        path: '/start',
        alias: '/',
        component: StartScreen
    },
    {
        path: '/question/:number',
        component: QuestionScreen
    },
    {
        path: '/results',
        component: ResultScreen
    }
]

export default new VueRouter({routes})
