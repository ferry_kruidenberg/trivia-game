import Vuex from 'vuex'
import Vue from "vue";
import {fetchUserAPI, patchUserAPI, postUserAPI} from "@/api/userAPI";
import {fetchQuestionsAPI} from "@/api/questionAPI";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        userObject: {},
        settings: {},
        questions: [],
        results: [],
        error: ''
    },

    mutations: {
        updateUser: (state, payload) => {
            state.userObject = payload
        },
        updateHighScore: (state, payload) => {
            state.userObject.highScore = payload
        },
        updateSettings: (state, payload) => {
            state.settings = payload
        },
        updateQuestions: (state, payload) => {
            state.questions = payload
        },
        updateResults: (state, payload) => {
            state.results = payload
        },
        updateAResult: (state, {payload, questionNumber}) => {
            state.results[questionNumber] = payload
        },
        updateError: (state, payload) => {
            state.error = payload
        }
    },

    getters: {
        numberOfQuestions: (state) => {
            return state.questions.length
        },
        getUser: (state) => {
            return state.userObject
        },
        getSettings: (state) => {
            return state.settings
        },
        getQuestion: (state) => (id) => {
            return state.questions[id]
        },
        getQuestions: (state) => {
            return state.questions
        },
        getResults: (state) => {
            return state.results
        }
    },

    actions: {
        async fetchUser({commit}, username) {
            const [error, user] = await fetchUserAPI(username)
            commit('updateError', error)
            commit('updateUser', user)
        },

        async postUser({commit}, username) {
            const [error, user] = await postUserAPI(username)
            commit('updateError', error)
            commit('updateUser', user)
        },

        async patchUser({commit}, {userId, score}) {
            const [error, user] = await patchUserAPI(userId, score)
            commit('updateError', error)
            commit('updateUser', user)
        },

        async fetchQuestions({commit}, {amount, difficulty, category}) {
            const [error, questions] = await fetchQuestionsAPI(amount, difficulty, category)
            commit('updateError', error)
            commit('updateQuestions', questions)
        }

    }
})


