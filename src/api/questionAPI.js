const apiURL = "https://opentdb.com"

export const fetchQuestionsAPI = async (amount, difficulty = '', category = '') => {
    try {
        const {results} = await fetch(`${apiURL}/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}`)
            .then(response => response.json())
        return [null, results]

    } catch (e) {
        return [e.message, []]
    }
}

