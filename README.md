# trivia_game

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## Project explanation

A single page application written in Vue.

Input the following:

- Username
- Number of questions
- Category
- Difficulty

When all questions are answered the user is given a list with all questions answered showing which were answered correct/incorrect.

If the results are better than previous results, they are sent to the database.